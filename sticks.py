#!/usr/bin/ipython

"""
File: tracks.py
Author: Adam Fuller
Date: 4/9/2016
Description: Read a joystick and send commands to a tracked robot.
"""

from radios import rfm69
import time
import struct
import js_linux
import web
import multiprocessing as mp
import argparse
import math
import Adafruit_BBIO.GPIO as GPIO
import lipo
import itertools
from collections import OrderedDict
import LEDs
import sys
import select
import re

led = {}
for i in range(4):
	led[i] = LEDs.LED(i)
	led[i].off()
	LEDs._settrigger(i, 'none')

parser = argparse.ArgumentParser(description=('Robot base station transciever.'))

parser.add_argument('--web', action='store_true',
	help = ('Serve an HTML/Websockets UI for pathfinding.'))

parser.add_argument('--port', type=int, default=8081,
	help = ('The port on which to serve the --websocket UI.'))

parser.add_argument('--debug', action='store_true',
	help = ('Do setup but don\'t start sending loop.'))

parser.add_argument('--exponent', type=float, default=1.0,
	help = ('The exponential term for joystick deflection.'))

parser.add_argument('--hexponent', type=float, default=1.0,
	help = ('The exponential term for steering input.'))

parser.add_argument('--style', type=str, default='left',
	help = ('Choose the steering style.'
	'"left" uses only the left stick.'
	#'"right" uses only the right stick.'
	'"twin" uses one stick per track.  "'
	'"GT" uses the left stick for steering, and the right stick for forward/back.'
	'"compass" uses the left stick to demand a heading and speed.'
	))

parser.add_argument('--control', type=str, default='openloop',
	help = ('Choose the control type.'
	'"openloop" sends basically a voltage demand.'
	'"closedloop" sends basically a speed demand.'
	))

parser.add_argument('--deadzone', type=float, default=50,
	help = ('The minimum output joystick deflection [0,255].'))

args = parser.parse_args()

print 'args.deadzone:', args.deadzone

# The rfm69 default CSMA (RSSI and time) values are sometimes too
# strict on background noise.  This increases the tolerance for
# background RSSI before transmitting (up from -90 dBm to -70 dBm)
# and decreases the delay to wait before a send attempt is made
# anyway (from 1000 ms to 100 ms).
rfm69.RF69_CSMA_LIMIT_MS = 100
rfm69.CSMA_LIMIT = -70

# RFM69 network settings.
node_id_of_this_node = 1
node_id_of_the_robot = 2
NETWORKID = 100
FREQUENCY = rfm69.RF69_433MHZ
IS_RFM69HW = True

j = js_linux.joystick()

print "Initializing radio"

RST = 'P9_12'

def reset_radio():
	GPIO.setup(RST, GPIO.OUT)
	GPIO.output(RST, GPIO.LOW)
	GPIO.output(RST, GPIO.HIGH)
	GPIO.output(RST, GPIO.LOW)

reset_radio()

radio = rfm69.RFM69()
radio.initialize(FREQUENCY,node_id_of_this_node,NETWORKID)
if IS_RFM69HW:
	radio.setHighPower(IS_RFM69HW) #only for RFM69HW!
radio.setEncryptionKey(None)
radio.setPromiscuous(False)

def request_status():
	led[0].blink(n=2, ton=0.020, toff=0.020)
	radio.send(node_id_of_the_robot, StatusRequest.pack({}), StatusRequest.calcsize(), False)

class dpack():

	def __init__(self, keys, f, typeid):

		"""
		A simple type of data packet based on a sort of ordered dict.

		Initialized with an iterable of keys, and a format string (see
		struct).
		"""

		assert len(keys) == len(f)

		self.typeid = typeid

		# Whatever type of packet is described, add a special first
		# expected byte called "typeid" to it which will be used to
		# identify this type of packet.
		keys = ['typeid']+keys
		f = 'B'+f

    # dict of format specifiers (see struct)
		self.f = OrderedDict(zip(keys, f))

		# dict of values
		self.v = OrderedDict(zip(keys, [None]*len(keys)))

	def __getitem__(self, k):

		"""
		Provide k,v access to last received values.
		"""

		return self.v[k]

	def __setitem__(self, k):

		"""
		Not implemented.  Use unpack to load data from a packet into
		self.v.
		"""

		raise Exception(NotImplemented)

	def format(self):

		"""
		Return the format string.
		"""

		return ''.join([v for k,v in self.f.iteritems()])

	def asstring(self):

		"""
		Format the current values of self.v.
		"""

		s = ''
		for k,v in self.v.iteritems():
			s+='\t'
			s+=k
			s+=': '
			s+=str(v)

		return s

	def asdict(self):

		return dict(self.v)

	def calcsize(self):

		"""
		Return the length of the packet in bytes.
		"""

		return struct.calcsize(self.format())

	def check(self, p):

		# Sniff the first byte.
		if self.typeid != p[0]:
			return False

		# Check that the message length is correct.
		if len(p) != self.calcsize():
			return False

		return True

	def unpack(self, p, ret=False):

		"""
		Unpack an ordered iterable of integers [0,255] values p
		according to this packet's keys.  If ret, return as a dict,
		else store the result in self.v.
		"""

		# Check the packet length.
		if self.check(p) == False:
			return False

		# Timestamp.  Why?  Why not?
		self.t = time.time()

		r = OrderedDict(zip(self.v.keys(), struct.unpack(self.format(),
			struct.pack(len(p)*'B', *p))))

		if ret:

			return r

		else:

			self.v = r
			
			return True

	def pack(self, d):

		"""
		Take the values in dict d and produce a packet according to
		self.format().  Return that packet as a string ready to
		transmit.  Does not modify the dpack object.
		"""

		d['typeid'] = self.typeid

		return struct.pack(self.format(), *[d[k] for k in self.f])

	def keys(self):
		return self.f.keys()

# A packet representing the speed demand of left and right
# tracks as signed ints.
Sticks = dpack(['vL', 'vR'], 'll', typeid=1)

# A packet to request an odometer reset on the robot.
ResetOdometer = dpack([], '', typeid=2)

# A packet to request a status reply from the robot.
StatusRequest = dpack([], '', typeid=3)

TransmitterPower = dpack(['power'], 'l', typeid=4)

# A packet to request a status reply from the robot.
SlewToNorth = dpack([], '', typeid=5)

# A packet representing the speed demand of left and right
# tracks as signed ints.
CompassSticks = dpack(['heading', 'v'], 'ff', typeid=6)

# A packet representing the speed demand of left and right
# tracks as signed ints.
AbsSticks = dpack(['vL', 'vR'], 'll', typeid=7)

# A packet to request an emergency stop of the robot.
EmergencyStop = dpack([], '', typeid=8)

# This is a type of packet that gets sent to the robot.
# posAt and posRt are positions in metres.
# origin determines how they are interpreted.  Valid values are:
#  'a' mean absolute.  Ask the robot to drive to this position relative
#  to its understanding of where 0,0 is, and according to the
#  global 'ahead' and 'right' directions.
#  'r' means relative.  Like 'a' but relative to its current
#  location rather than 0,0.
#  'v' means vehicle.  Like 'r' but 'ahead' means 'forward along the
#  current heading' and 'right' means 'right relative to the
#  current heading'.
TargetUpdate = dpack(['posAt', 'posRt', 'origin'], 'ffc', typeid=9)

# This is a type of packet that gets received from the robot.
StatusPacket = dpack(['RSSIRx', 'RSSIbg',
#	'latitude', 'longitude',
	'posA', 'posR', 'Vbat', 'heading'],
	'llffff',
	#'llLLffff',
	typeid=10)

# Position update.  robot transmission.
PositionUpdate = dpack(['posA', 'posR'], 'ff', typeid=11)

# A packet to request a wheelie.
DoWheelie = dpack(['k1', 'k2', 'k3'], 'fff', typeid=12)

# A packet to request roving.
# V is a target speed [0,255] while roving.
# t is a duration in seconds.
Rove = dpack(['V', 't'], 'fl', typeid=13)


def sticks(writable=None):

	"""
	A function including an infinite loop to transmit commands
	(initially joystick positions) to a robot.

	Pass a server object to allow broadcasting of status data
	received from the robot.
	"""

	heading_offset = 0

	i = 0
	power_level = 31
	radio.setPowerLevel(power_level)

	doSend = True

	#expected_format = 'leftahead'
	expected_format = 'status'

	# These allow for runtime compensation of motor wiring polarity.
	flip_left_motor = False
	flip_right_motor = False
	fsteer = 1
	styles = itertools.cycle(['left', 'GT', 'twin', 'compass'])
	controls = itertools.cycle(['openloop', 'closedloop'])
	style = args.style
	control = args.control

	while True:

		if sys.stdin in select.select([sys.stdin], [], [], 0)[0] and True:

			x = sys.stdin.readline().strip()

			print "stdin provided:", x

			if re.match('[arv] [-+]?[0-9]*\.?[0-9]+ [-+]?[0-9]*\.?[0-9]+', x):

				print "Matched position target."

				origin, posAt, posRt = x.split(' ')
				pack = TargetUpdate.pack({'posAt': float(posAt),
					'posRt':float(posRt), 'origin': origin})
				radio.send(node_id_of_the_robot, pack, len(pack), False)

			elif re.match('w [-+]?[0-9]*\.?[0-9]+ [-+]?[0-9]*\.?[0-9]+ [-+]?[0-9]*\.?[0-9]+', x):

				print "Matched wheelie."

				bob, k1, k2, k3 = x.split(' ')

				radio.send(node_id_of_the_robot,
						DoWheelie.pack({'k1': float(k1), 'k2': float(k2), 'k3':
							float(k3)}), DoWheelie.calcsize(), False)


			elif re.match('rove [-+]?[0-9]*\.?[0-9]+ [0-9]+', x):

				print "Matched rove."

				bob, V, t = x.split(' ')

				radio.send(node_id_of_the_robot,
						Rove.pack({'V': float(V), 't': int(t)}), Rove.calcsize(), False)

			else:

				print "Input not recognized."

		"""
		Get input and detect any rising edges.
		"""

		js = j.state

		Leftd = js[0]['D-pad left']
		if Leftd and not oldLeftd:
			print "Left."
			pack = TargetUpdate.pack({'posAt': 0.0, 'posRt':-0.1,
				'origin': 'r'})
			radio.send(node_id_of_the_robot, pack, len(pack), False)
		oldLeftd = Leftd

		Rightd = js[0]['D-pad right']
		if Rightd and not oldRightd:
			print "Right."
			pack = TargetUpdate.pack({'posAt': 0.0, 'posRt':0.1,
				'origin': 'r'})
			radio.send(node_id_of_the_robot, pack, len(pack), False)
		oldRightd = Rightd

		Upd = js[0]['D-pad up']
		if Upd and not oldUpd:
			print "Up."
			pack = TargetUpdate.pack({'posAt': 0.1, 'posRt':0,
				'origin': 'r'})
			radio.send(node_id_of_the_robot, pack, len(pack), False)
		oldUpd = Upd

		Downd = js[0]['D-pad down']
		if Downd and not oldDownd:
			print "Down."
			pack = TargetUpdate.pack({'posAt': -0.1, 'posRt':0,
				'origin': 'r'})
			radio.send(node_id_of_the_robot, pack, len(pack), False)
		oldDownd = Downd

		Circle = js[0]['Circle']
		if Circle and not oldCircle:
			print "Style switching."

			style = styles.next()
			print "Switched style to", style

		oldCircle = Circle

		Ex = js[0]['Ex']
		if Ex and not oldEx:
			print "Control switching."

			control = controls.next()
			print "Switched control to", control

		oldEx = Ex

		Triangle = js[0]['Triangle']
		if Triangle and not oldtri:
			print "Requesting odometer reset."
			radio.send(node_id_of_the_robot, ResetOdometer.pack({}), ResetOdometer.calcsize(), False)
			heading_offset = 0
		oldtri = Triangle

		Sq = js[0]['Square']
		if Sq and not oldSq:
			#print "Requesting status."
			expected_format = 'status'
			request_status()
		oldSq = Sq

		Start = js[0]['Start']
		if Start and not oldStart:
			if doSend:
				print "Pausing sending."
				doSend = False
			else:
				print "Commencing sending."
				doSend = True
		oldStart = Start

		L1 = j.state[0]['L1']
		if L1 and not oldL1:
			radio.send(node_id_of_the_robot, EmergencyStop.pack({}), EmergencyStop.calcsize(), False)
			print "Requesting E-stop."
		oldL1 = L1

		L2 = j.state[0]['L2']
		if L2 and not oldL2:
			radio.send(node_id_of_the_robot, DoWheelie.pack({'k1':999, 'k2': 999, 'k3': 999}), DoWheelie.calcsize(), False)
			print "Requesting wheelie."
		oldL2 = L2

		R1 = j.state[0]['R1']
		if R1 and not oldR1 and power_level<31:
			power_level += 1
			#radio.setPowerLevel(power_level)
			pack = TransmitterPower.pack({'power': power_level})
			radio.send(node_id_of_the_robot, pack, len(pack), False)
			print "Transmitter power levels increased to {}".format(power_level)
		oldR1 = R1

		R2 = j.state[0]['R2']
		if R2 and not oldR2 and power_level>0:
			power_level -= 1
			#radio.setPowerLevel(power_level)
			pack = TransmitterPower.pack({'power': power_level})
			radio.send(node_id_of_the_robot, pack, len(pack), False)
			print "Transmitter power levels decreased to {}".format(power_level)
		oldR2 = R2

		L3 = js[0]['L3']
		if L3 and not oldL3:
			print "Changing motor flips."
			flip_left_motor, flip_right_motor = {
					(True, True): (True, False),
					(True, False): (False, True),
					(False, True): (False, False),
					(False, False): (True, True),
					}[(flip_left_motor, flip_right_motor)]
		oldL3 = L3

		R3 = js[0]['R3']
		if R3 and not oldR3:
			print "Changing steering polarity."
			fsteer = {1: -1, -1: 1}[fsteer]
			print "fsteer", fsteer
		oldR3 = R3


		"""
		Prepare the sticks data.
		vL gets sent to the left motor.
		vR gets sent to the right motor.
		"""

		if style in ['left']:

			forward = js[1]['Left stick, down']*-1
			steer = js[1]['Left stick, right']

			vL = int(255.*(forward + 1.0*fsteer*steer))
			vR = int(255.*(forward - 1.0*fsteer*steer))

		elif style == 'twin':

			if fsteer>0:

				vL = int(255.*(js[1]['Left stick, down']))*-1
				vR = int(255.*(js[1]['Right stick, down']))*-1

			else:

				vR = int(255.*(js[1]['Left stick, down']))*-1
				vL = int(255.*(js[1]['Right stick, down']))*-1

		elif style == 'GT':

			# Drives more like a car.

			forward = js[1]['Right stick, down']*-1
			steer = js[1]['Left stick, right']

			if forward > 0:

				vL = int(255.*(max(0, forward + 1.0*fsteer*steer*forward)))
				vR = int(255.*(max(0, forward - 1.0*fsteer*steer*forward)))

			else:

				vL = int(255.*(min(0, forward + 1.0*fsteer*steer*forward)))
				vR = int(255.*(min(0, forward - 1.0*fsteer*steer*forward)))

		else:

			pass

		if style == 'compass':

			"""
			Transmit a compass heading and speed to the robot.
			"""

			Up = js[1]['Left stick, down']*-1.0
			Right = js[1]['Left stick, right']*1.0

			# This computes the angle CW from up of the joystick.
			heading = math.degrees(math.atan2(Right, Up))

			# This adds the last known heading offset to the joystick
			# angle.  This allows any actual robot heading to correspond
			# to "up" on the controller joystick.
			heading += heading_offset

			# This makes for a nice compass angle.
			heading %= 360

			# This clips the joystick corner magnitudes of 1..1.41 to 1.
			v = min(1.0, (Up**2 + Right**2)**0.5)

			if abs(v)<(args.deadzone/255.):
				v = 0

			if i % 10 == 0:
				print "Up, Right, heading, v =\t{}\t{}\t{}\t{}".format(Up, Right, heading, v)

			if doSend:
				led[0].shot()
				pack = CompassSticks.pack({'heading': heading, 'v': v})
				radio.send(node_id_of_the_robot, pack, len(pack), False)

		else:

			"""
			Transmit a target speed.
			"""

			vL = min(255, max(-255, vL))
			vR = min(255, max(-255, vR))

			if abs(vL)<args.deadzone:
				vL=0
			if abs(vR)<args.deadzone:
				vR=0

			if flip_left_motor:
				vL = -vL

			if flip_right_motor:
				vR = -vR

			if args.exponent:
				vL = int(math.copysign(vL,vL)*abs(vL)**args.exponent / 255.0**args.exponent)
				vR = int(math.copysign(vR,vR)*abs(vR)**args.exponent / 255.0**args.exponent)


			"""
			Transmit a "sticks" packet to the robot.
			"""

			if control == 'closedloop':
				pack = AbsSticks.pack({'vL': vL, 'vR': vR})
			elif control == 'openloop':
				pack = Sticks.pack({'vL': vL, 'vR': vR})
			else:
					raise Exception('Control must be openloop or closedloop.')
			if doSend:
				led[0].shot()
				radio.send(node_id_of_the_robot, pack, len(pack), False)


			if i % 20 == 0:
				if doSend:
					print "vL, vR =", vL, vR
				#else:
					#print "vL, vR =", vL, vR, "(Sending paused.)"

		"""
		Listen for one packet from the robot for a while.  If one is
		ready, process it.
		"""
		tstart = time.time()
		while time.time() - tstart < 0.05: # duration of listen.

			# Check for a packet received.
			if radio.receiveDone():

				if radio.ACK_REQUESTED:

					radio.sendACK()

				d = radio.DATA

				#print d
				#print ''.join(map(lambda x: '{:02x}'.format(x), d))

				"""
				print 
				print "radio.SENDERID", radio.SENDERID
				print "radio.TARGETID", radio.TARGETID
				print "radio.PAYLOADLEN", radio.PAYLOADLEN
				print "radio.DATALEN", radio.DATALEN
				print "radio.ACK_REQUESTED",radio.ACK_REQUESTED
				print "radio.RSSI", radio.RSSI
				print "len(d)", len(d)
				print "d", d
				print
				"""

				if d[0] == 10:

					#print "Status packet received."

					StatusPacket.unpack(d)

					r = StatusPacket.v.copy()

					print StatusPacket.asstring()
					
					if 'latitude' in r and 'longitude' in r:
						for k in ['latitude', 'longitude']:
							r[k] /= 1e7

					r['SOC'] = lipo.charge(r['Vbat'])

					if style != 'compass':

						# If we're not already in 'compass' mode, remember the
						# current robot heading.  With this, it is useful to
						# request a robot status packet prior to switching to
						# compass mode, so that moving in the initial robot
						# heading will correspond to pressing "up" on the
						# joystick.
						heading_offset = r['heading']
						#print "heading_offset set to r['heading']."

					if writable:

						writable.send(StatusPacket.asdict())

				elif d[0] == 11:

					print "Position update received."

					d = radio.DATA

					r = PositionUpdate.unpack(d)

					print PositionUpdate.asstring()

					if writable:

						writable.send(PositionUpdate.asdict())

				else:

					print "Packet received that I don't know about, typeid=", d[0]

				break # Break out of listen while loop.

		i += 1

if args.debug:

	pass

else:

	if args.web:
		import SimpleWebSocketServer as swss
		import json

		readable, writable = mp.Pipe(False)

		class index:
			def GET(self):
				return open('index.html')

		def server():

			# An HTTP server to serve the HTML.

			app = web.application(('/', 'index'), globals())
			web.httpserver.runsimple(app.wsgifunc(), ("0.0.0.0", args.port))




		class WS(swss.WebSocket):

			def send(self, o):
				s = unicode(json.dumps(o))
				print "sending:", s
				self.sendMessage(s)

			def handleMessage(self):
				print self.data
				try:
					message = json.loads(self.data)
				except:
					print "Are you sure the packet was JSON?"
				print message

			def handleConnected(self):
				print self.address, 'connected'

			def handleClose(self):
				print self.address, 'closed'

			def handleError(self):
				print "We had a problem."
				print self

		# Initiate a server with the WS class on a different port to
		# the HTTP server.  I think every connection to this server
		# will spawn a new instance of this class.
		wss = swss.SimpleWebSocketServer('', args.port+1, WS)

		def wsserver(readable):

			wss.serveforever(readable)

		p_sticks = mp.Process(target=sticks, args=(writable,))
		p_server = mp.Process(target=server)
		p_wsserver = mp.Process(target=wsserver, args=(readable,))

		p_sticks.start()
		p_wsserver.start()
		p_server.start()
		print "All systems are go."

	else:

		sticks()
