#include <RFM69.h> //get it here: https://www.github.com/lowpowerlab/rfm69
#include <RFM69registers.h> //get it here: https://www.github.com/lowpowerlab/rfm69
#include <SPI.h>
#include <Adafruit_MotorShield.h>
#include <Adafruit_GPS.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <NewPing.h>

int cdebug = ' '; // A way to select debug info to print.

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *mL = AFMS.getMotor(3);
Adafruit_DCMotor *mR = AFMS.getMotor(4);
Adafruit_BNO055 bno = Adafruit_BNO055();

adafruit_bno055_offsets_t newCalib;

void displaySensorOffsets(const adafruit_bno055_offsets_t &calibData)
{
  Serial.print("Accelerometer: ");
  Serial.print(calibData.accel_offset_x); Serial.print(" ");
  Serial.print(calibData.accel_offset_y); Serial.print(" ");
  Serial.print(calibData.accel_offset_z); Serial.print(" ");

  Serial.print("\nGyro: ");
  Serial.print(calibData.gyro_offset_x); Serial.print(" ");
  Serial.print(calibData.gyro_offset_y); Serial.print(" ");
  Serial.print(calibData.gyro_offset_z); Serial.print(" ");

  Serial.print("\nMag: ");
  Serial.print(calibData.mag_offset_x); Serial.print(" ");
  Serial.print(calibData.mag_offset_y); Serial.print(" ");
  Serial.print(calibData.mag_offset_z); Serial.print(" ");

  Serial.print("\nAccel Radius: ");
  Serial.print(calibData.accel_radius);

  Serial.print("\nMag Radius: ");
  Serial.print(calibData.mag_radius);
}

#define mLpolarity -1
#define mRpolarity -1
#define WheelieMillis 10

#define maxVd 200 // Max motor output [0,255]

int previous_node = 0;  // used to detect first pass through e.g. "speed" loop.

boolean sendpos = false;
int lastsend = millis();

float alpha = 0.2;
float alphav = 0.2;
float ki = 0.2;
float kp = 3.0;
float powerL, powerR = 0;
float powerLpterm, powerLiterm, powerLprev = 0;
float powerRpterm, powerRiterm, powerRprev = 0;
bool inManeuver = false; // this variable should be set to true if an automatic maneuver is underway, otherwise false.
bool inSlew = false; // this variable should be set to true if an automatic slew is underway, otherwise false.
bool inRove = false; // this variable should be set to true if actively roving, otherwise be false.
float poserror = 0;
float heading_to_target = 0;
float heading_error = 0;
float poserrmag = 0;
float Vtarget = 0;

long t_start;

#define NETWORKID     100  // The same on all nodes that talk to each other
#define NODEID        2    // The unique identifier of this node
#define BASE          1    // The unique identifier of the base station.
#define FREQUENCY     RF69_433MHZ
#define ENCRYPTKEY    0 //exactly the same 16 characters/bytes on all nodes!
#define IS_RFM69HCW   true // set to 'true' if you are using an RFM69HCW module
#define MELincrement  1 // should be 1 or -1.
#define MERincrement  1 // should be 1 or -1.
#define ticks_per_meter  1300.0 // encoder ticks per meter of forward travel [m^-1]
#define ltracks     0.110 // distance between tracks [m]
#define rad_to_deg  180/3.14159
#define deg_to_rad  3.14159/180
#define GPSSerial Serial1

#define PWMf 1600

//*********************************************************************************************
#define SERIAL_BAUD   115200

#define RFM69_CS      8
#define RFM69_IRQ     3
#define RFM69_INT RFM69_IRQ // This is only a new name because I'm working on a branch of RFM69 without interrupts where I'll need to poll the pin directly.
#define RFM69_IRQN    3  // Pin 3 is IRQ 3!
#define RFM69_RST     4
#define LED           13
#define VBATPIN       A7


#define radiotimeout  250 // software radio reception timeout [ms]  If we're not in a maneuver, if no data is received for this long assume we've lost comms and stop the tracks.
#define dMmax 1.024 // maximum motor demand change per millisecond.

// Motor quadrature encoders
#define MERB        A1 // Port for A1 is PB08 PORTB
// Do not use A2 for an interrupt!
// Something else in this code must be hitting it.
// A0, A2, A3, A4, and A5 all seem to work fine for the
// purpose of the wheel encoder ISRs.
#define MERA        A3 // Port for A3 is PA04
#define MELB        A4 // Port for A4 is PA05
#define MELA        A5 // Port for A5 is PB02


#define PINGTRIG 13
#define PINGECHO0 12
#define PINGECHO1 11
NewPing sonar0(PINGTRIG, PINGECHO0, 100);
NewPing sonar1(PINGTRIG, PINGECHO1, 200);


#define READMELB (int)((*(int*)0x60000020)>>5 & 1)>0 // MELB is at Pin A4 = Port PA05.  THIS DOESN'T WORK!
#define READMERB (int)((*(int*)0x600000a0)>>8 & 1)>0 // MERB is at Pin A1 = Port PB08.  THIS DOESN'T WORK!


bool Radiopresent, IMUpresent, GPSpresent;

RFM69 radio = RFM69(RFM69_CS, RFM69_IRQ, IS_RFM69HCW, RFM69_IRQN);


Adafruit_GPS GPS(&GPSSerial);

void blink(long n) {
  while (n--) {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(200);                       // wait for a second
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    delay(100);                       // wait for a second
  }
}

typedef struct {
  uint8_t packid = 1;
  long vL;
  long vR;
} Sticks;

typedef struct {
  uint8_t packid = 3;
  char sub;
  long t = 1000;
} RequestStatus;

typedef struct {
  uint8_t packid = 4;
  long power;
} TransmitterPower;

typedef struct {
  uint8_t packid = 6;
  float heading;
  float v;
} CompassSticks;

typedef struct {
  uint8_t packid = 7;
  long vL;
  long vR;
} AbsSticks;

typedef struct {
  uint8_t packid = 9;
  float posAt;
  float posRt;
  char origin;
} TargetUpdate;

typedef struct {
  uint8_t packid = 14;
  float heading;
  char origin;
} SlewTarget;

typedef struct {
  uint8_t packid = 12;
  float k1;
  float k2;
  float k3;
} DoWheelie;

typedef struct {
  uint8_t packid = 13;
  float V;
  long t;
} Rove;

typedef struct {
  uint8_t packid = 10; // sticks.py is now checking the first byte as a packet type/format identifier.
  int32_t RSSIRx = 9999;
  int32_t RSSIbg = 9999;
  float posA = 0.0;
  float posR = 0.0;
  float Vbat = 99.99;
  float heading = 99.99;
  int32_t cmclearahead = 9999;
  int32_t cmclearleft = 9999;
} packet;

typedef struct {
  uint8_t packid = 11;
  float posA;
  float posR;
} PositionUpdate;

void print_n_bytes(const char *p, uint8_t n) {
  // Holy crap this function works fine!
  // To use it to print a "payload" struct, do this:
  // print_n_bytes((const char*)(&payload), sizeof(payload));
  for (uint8_t i = 0; i < n; i++) Serial.print(p[i], HEX);  // Prints bytes as a
}

packet payload;
RequestStatus requeststatus;
TargetUpdate tuunpack;
SlewTarget stunpack;
Sticks vnew;
AbsSticks vnew2;
CompassSticks absvnew;
PositionUpdate posupdate;
DoWheelie dw;
Rove vRove;
int i = 1;
long lastreceipt = millis();
volatile long vLold, vRold, vLnew, vRnew;
long vL, vR;
volatile long dt;
long speed;
volatile long tlastL, tlastR = micros(); // timestamp of latest encoder trips [us]
volatile long dtL, dtR = 0; // time between latest and previous encoder clicks [us]
volatile float vLm, vRm = 0; // measured track velocities [m/s]
volatile float vLms, vRms, vLmsprev, vRmsprev = 0; // synchronous measured track velocity. [m/s].
volatile int32_t odoLprev, odoRprev = 0;
long oldtIMU = 0; // millis() at previous IMU sample.
long dtIMUtarget = 50; // target IMU sample duration [ms].
long dtIMUactual = 0; // measured IMU sample duration [ms].
float Vx = 0;
imu::Vector<3> l;

float errorL, errorR = 0;

int vLmNC, vRmNC;
float vLmprev, vRmprev;

// Dead reckoning variables.
volatile int32_t odoL = 0;
volatile int32_t odoR = 0;
volatile float heading, headingi = 0; // Calculated heading, odometer- and IMU-based.
volatile float posA, posR, posAi, posRi = 0.0;  // Calculated position ahead/right, odometer- and IMU-based.
float posAt, posRt, posAerr, posRerr = 0;
volatile uint8_t x;
long PositionUpdatetimeout = 200; // Minimum duration between automatic position update transmissions.  [ms]
#define HEADINGI headingi*deg_to_rad // IMU heading is in degrees.


float vmax = 1.0; // An attainable maximum track velocity [m/s]
float vmin = 0.001; // Track velocities below this are assumed zero. [m/s]


//Band stop chebyshev filter order=2 alpha1=0.02 alpha2=0.024 
class  FilterChBs2
{
  public:
    FilterChBs2()
    {
      for(int i=0; i <= 4; i++)
        v[i]=0.0;
    }
  private:
    float v[5];
  public:
    float step(float x) //class II 
    {
      v[0] = v[1];
      v[1] = v[2];
      v[2] = v[3];
      v[3] = v[4];
      v[4] = (9.884637148378466742e-1 * x)
         + (-0.97736844629304953092 * v[0])
         + (3.89403249647758142871 * v[1])
         + (-5.85588531267496215094 * v[2])
         + (3.93886739835358934414 * v[3]);
      return 
         1.000000 * v[0]
        +v[4]
        - 3.962159 * v[1]
        - 3.962159 * v[3]
        +5.924675 * v[2];
    }
};


FilterChBs2 *f;


void onMELAfall() {
  long t = micros();
  dtL = t - tlastL;
  tlastL = t;
  if (digitalRead(MELB)) {
    odoL += MELincrement;
    heading += MELincrement / ltracks / ticks_per_meter;
    posA += 0.5 * cos(HEADINGI) / ticks_per_meter;
    posR += 0.5 * sin(HEADINGI) / ticks_per_meter;
    //Serial.println("L+");
    vLm = 1000000.0 / ((float)ticks_per_meter * (float)dtL); // [tick/m] * [m/s]
  }
  else {
    odoL -= MELincrement;
    heading -= MELincrement / ltracks / ticks_per_meter;
    posA -= 0.5 * cos(HEADINGI) / ticks_per_meter;
    posR -= 0.5 * sin(HEADINGI) / ticks_per_meter;
    //Serial.println("L-");
    vLm = -1000000.0 / ((float)ticks_per_meter * (float)dtL); // [tick/m] * [m/s]
  }
  if (abs(vLm) < vmin) {
    vLm = 0;
  }
}

void onMERAfall() {
  long t = micros();
  dtR = t - tlastR;
  tlastR = t;
  if (digitalRead(MERB)) {
    odoR += MERincrement;
    heading -= MERincrement / ltracks / ticks_per_meter;
    posA += 0.5 * cos(HEADINGI) / ticks_per_meter; // when calculating displacements, use the IMU for heading and odometry for movement.
    posR += 0.5 * sin(HEADINGI) / ticks_per_meter;
    //Serial.println("R+");
    vRm = 1000000.0 / ((float)ticks_per_meter * (float)dtR); // [tick/m] * [m/s]
  }
  else {
    odoR -= MERincrement;
    heading += MERincrement / ltracks / ticks_per_meter;
    posA -= 0.5 * cos(HEADINGI) / ticks_per_meter;
    posR -= 0.5 * sin(HEADINGI) / ticks_per_meter;
    //Serial.println("R-");
    vRm = -1000000.0 / ((float)ticks_per_meter * (float)dtR); // [tick/m] * [m/s]
  }
  if (abs(vRm) < vmin) {
    vRm = 0;
  }
}

float Vbat() {

  /* Thanks Adafruit:
    https://learn.adafruit.com/adafruit-feather-m0-radio-with-rfm69-packet-radio/overview?view=all#measuring-battery
  */

  float measuredvbat = analogRead(VBATPIN);
  measuredvbat *= 2;    // we divided by 2, so multiply back
  measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
  measuredvbat /= 1024; // convert to voltage
  return measuredvbat;
}

void set_speed(Adafruit_DCMotor *motor, long speed) {
  if (speed > 0) {
    motor->run(FORWARD);
  } else if (speed < 0) {
    motor->run(BACKWARD);
  } else {
    motor->run(RELEASE);
  }

  motor->setSpeed(abs(speed));
}

float azerror(float target, float heading) {
  // units are degrees.
  float error;

  error = target - heading;

  if (error < -180) {
    error += 360;
  } else if (error > 180) {
    error -= 360;
  }

  return error;
}

void setup() {
  blink(2);
  Serial.begin(SERIAL_BAUD);
  f = new FilterChBs2();

  AFMS.begin(PWMf);  // sets the motor driver PWM frequency [Hz], max 1600 Hz, min ?
  set_speed(mL, 0);
  set_speed(mR, 0);


  long t;
  t = millis();

  while (!Serial and ((millis() - t) < 5000)) {} // Wait for Serial to open or 5 seconds to expire, whichever happens first.

  GPS.begin(9600);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ); // 1 Hz update rate

  // Give the GPS unit 100 ms to make itself known (tested and takes ~5 ms typical).
  t = millis();
  GPSpresent = true;
  //GPS.standby();// wait for library to believe the GPS is in standby.
  //GPSpresent = GPS.wakeup(); // returns true if special AWAKE sentence is received.
  Serial.print("GPSpresent=");
  Serial.println(GPSpresent);

  pinMode(MELA, INPUT);
  pinMode(MELB, INPUT);
  pinMode(MERA, INPUT);
  pinMode(MERB, INPUT);

  //*(int*)0x60000024 = 0b11111111; // enable sampling for all pins on PORTA.  THIS SEEMS TO BE ESSENTIAL FOR GETTING UP-TO-DATE DATA IN DIRECT PORT READS.
  //*(int*)0x600000a4 = 0b11111111; // enable sampling for all pins on PORTB.  THIS SEEMS TO BE ESSENTIAL FOR GETTING UP-TO-DATE DATA IN DIRECT PORT READS.

  attachInterrupt(MELA, onMELAfall, FALLING);
  attachInterrupt(MERA, onMERAfall, FALLING);

  // Hard Reset the RFM module
  pinMode(RFM69_RST, OUTPUT);
  digitalWrite(RFM69_RST, HIGH);
  delay(100);
  digitalWrite(RFM69_RST, LOW);
  delay(100);

  // Initialize radio
  radio.initialize(FREQUENCY, NODEID, NETWORKID);
  if (radio.getFrequency()) {
    Radiopresent = true;
  } else {
    Radiopresent = false;
  }

  radio.writeReg(REG_BITRATEMSB, RF_BITRATEMSB_4800);
  radio.writeReg(REG_BITRATELSB, RF_BITRATELSB_4800);
  radio.writeReg(REG_FDEVMSB, RF_FDEVMSB_5000);
  radio.writeReg(REG_FDEVLSB, RF_FDEVLSB_5000);
  Serial.print("Radiopresent=");
  Serial.println(Radiopresent);

  if (IS_RFM69HCW) {
    radio.setHighPower();    // Only for RFM69HCW & HW!
  }
  radio.setHighPowerRegs(false);

  radio.setPowerLevel(31);
  
  radio.encrypt(ENCRYPTKEY);
  radio.promiscuous(false);

  if (!bno.begin(Adafruit_BNO055::OPERATION_MODE_NDOF)) {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    IMUpresent = false;
  } else {
    Serial.print("BNO055 started OK.");

    adafruit_bno055_offsets_t bno055_calibration;

    bno055_calibration.accel_offset_x = 65508;
    bno055_calibration.accel_offset_y = 65475;
    bno055_calibration.accel_offset_z = 65527;
    bno055_calibration.gyro_offset_x = 65535;
    bno055_calibration.gyro_offset_y = 0;
    bno055_calibration.gyro_offset_z = 65535;
    bno055_calibration.mag_offset_x = 263;
    bno055_calibration.mag_offset_y = 356;
    bno055_calibration.mag_offset_z = 65418;
    bno055_calibration.accel_radius = 1000;
    bno055_calibration.mag_radius = 735;

    bno.setSensorOffsets(bno055_calibration);
    bno.setMode(Adafruit_BNO055::OPERATION_MODE_CONFIG);
    bno.write8(Adafruit_BNO055::BNO055_AXIS_MAP_CONFIG_ADDR, 0b100001); // default zyx is 0b100100.
    bno.write8(Adafruit_BNO055::BNO055_AXIS_MAP_SIGN_ADDR, 0b000); // default positive, positive positive ix 0b000.
    bno.setMode(Adafruit_BNO055::OPERATION_MODE_NDOF);
    Serial.println(bno.read8(Adafruit_BNO055::BNO055_AXIS_MAP_CONFIG_ADDR));

    delay(1000);
    Serial.print("BNO055 fully calibrated?:");
    Serial.println(bno.isFullyCalibrated());
    IMUpresent = true;
  }
  Serial.print("IMUpresent=");
  Serial.println(IMUpresent);

  radio.receiveDone();

  blink(1);

}

void loop() {
  GPS.read();

  if (GPS.newNMEAreceived()) {
    //Serial.println(GPS.lastNMEA());
    GPS.parse(GPS.lastNMEA());

    imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);

    //    payload.latitude_fixed = GPS.latitude_fixed;
    //    payload.longitude_fixed = GPS.longitude_fixed;

  }

  /*
    Serial.print(vLm);
    Serial.print(" ");
    Serial.println(vRm);
  */

  if (radio.receiveDone()) {

    if (radio.ACKRequested()) {
      Serial.println("Sending ACK.");
      delay(5);
      radio.sendACK();
    }

    payload.RSSIRx = radio.RSSI;

    //print_n_bytes((const char*)(&radio.DATA), sizeof(radio.DATA));

    if (cdebug == 'p') {
      // Radio pips
      Serial.println(radio.TARGETID);
    }

    //previous_node = radio.TARGETID;
    dt = millis() - lastreceipt;
    lastreceipt = millis();

    switch (radio.DATA[0]) {
      //switch (radio.TARGETID) {
      case 1:
        {
          inManeuver = inSlew = false; // manual input should cancel any maneuver.

          vnew = *(Sticks*)radio.DATA;
          vLnew = max(vLold - dMmax * dt, min(vLold + dMmax * dt, vnew.vL));
          vRnew = max(vRold - dMmax * dt, min(vRold + dMmax * dt, vnew.vR));
          set_speed(mL, vLnew * mLpolarity);
          set_speed(mR, vRnew * mRpolarity);
          vLold = vLnew;
          vRold = vRnew;

          previous_node = 1;
          break;
        }

      case 2:
        {
          Serial.println("Odometers reset.");
          set_speed(mR, 0);
          set_speed(mL, 0);
          blink(1);
          inManeuver = inSlew = false; // manual input should cancel any maneuver.
          odoL = 0;
          odoR = 0;
          heading = 0.0;
          headingi = 0.0;
          posA = 0.0;
          posR = 0.0;
          posAi, posRi = 0.0;
          Vx = 0;
          previous_node = 2;

          // .begin resets the BNO055 IMU.
          bno.begin(Adafruit_BNO055::OPERATION_MODE_NDOF);

          break;
        }

      case 3:
        {
          //sendpos PositionUpdatetimeout
          requeststatus = *(RequestStatus*)radio.DATA;
          Serial.print(requeststatus.sub);
          Serial.print('\t');
          Serial.print(requeststatus.t);
          Serial.println();

          if (requeststatus.sub == '1') {
            Serial.print("Sending status...");
  
            //payload.RSSIRx = radio.RSSI;
            payload.cmclearahead = NewPing::convert_cm(sonar0.ping_median());
            payload.cmclearleft = NewPing::convert_cm(sonar1.ping_median());
            payload.Vbat = Vbat();
            payload.posA = posA;
            payload.posR = posR;
            Serial.println();
            imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
            payload.heading = euler.x(); // Magnetic heading, not dead-reckoned.
            //payload.TxPower = (long)radio.readReg(REG_PALEVEL);
  
            if (radio.sendWithRetry(BASE, (const void*)(&payload), sizeof(payload), 10, 50)) Serial.println("OK."); else Serial.println("Silence.");
  
            /*
              Serial.print("payload.packid: ");
              Serial.print(payload.packid);
              Serial.print('\t');
              Serial.print("payload.Vbat: ");
              Serial.print(payload.Vbat);
              Serial.print('\t');
              Serial.print("payload.RSSIRx: ");
              Serial.print(payload.RSSIRx);
              Serial.print('\t');
              Serial.print("payload.RSSIbg: ");
              Serial.print(payload.RSSIbg);
              Serial.print('\t');
              Serial.print("payload.posA: ");
              Serial.print(payload.posA, 3);
              Serial.print('\t');
              Serial.print("payload.posR: ");
              Serial.print(payload.posR, 3);
              Serial.print('\t');
              Serial.print("payload.latitude: ");
              Serial.print(payload.latitude_fixed);
              Serial.print('\t');
              Serial.print("payload.longitude: ");
              Serial.print(payload.longitude_fixed);
              Serial.print('\t');
              Serial.print("payload.heading: ");
              Serial.print(payload.heading, 1);
              Serial.print('\t');
  
              Serial.println();
            */
  
  
            AFMS.begin(PWMf);  // sometimes the motor shields seems to stop responding.  Seeing if re-calling .begin can fix that.
  
  
            Serial.println();
            print_n_bytes((const char*)(&payload), sizeof(payload));
            Serial.println();
          } else if (requeststatus.sub == 'S') {
            sendpos = true;
            PositionUpdatetimeout = requeststatus.t;
          } else if (requeststatus.sub == 's') {
            sendpos = false;
          }

          previous_node = 3;
          break;
        }

      case 4:
        {
          // Set the transmitter power level (0 to 31) based on the received value.  Read the power level register back and send back to base.
          int x = (*(TransmitterPower*)radio.DATA).power;
          radio.setPowerLevel(min(31, max(0, x))); // power output ranges from 0 (5dBm) to 31 (20dBm)
          Serial.print("Power level set to ");
          Serial.print(x);
          Serial.println();
          previous_node = 4;
          break;
        }

      case 5:
        {
          // Special packet requesting slew to North received.

          inManeuver = inSlew = false; // manual input should cancel any maneuver.

          // Sample Euler angles from IMU.
          imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);

          float target = 0.0; // target heading north
          float error;

          long tstart = millis();

          uint8_t system, gyro, accel, mag;
          system = gyro = accel = mag = 0;
          bno.getCalibration(&system, &gyro, &accel, &mag);

          Serial.println(system);
          Serial.println(gyro);
          Serial.println(accel);
          Serial.println(mag);
          Serial.println();

          // Heading error minimizing loop
          //if (mag > 0) { // this requires the magnetometer to be calibrated.
          if (true) { // this assumes heading is OK e.g. if mode is IMUPLUS where the gyro is used to give a heading relative to startup.
            while (true) {


              // Sample Euler angles from IMU.
              euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);

              // Get current heading.
              heading = euler.x();

              error = azerror(target, heading);

              speed = (long)max(-100, min(100, error * 20.0));

              set_speed(mR, speed);
              set_speed(mL, -speed);

              if (abs(error) < 10.0) {
                Serial.println("Slewing complete.");

                break;
              }

              if ((millis() - tstart) > 2000) {
                Serial.println("Slewing timed out.");
                break;
              }
            }
          } else Serial.print("Magnetometer not calibrated.");

          // Stop both tracks.
          set_speed(mL, 0);
          set_speed(mR, 0);

          Serial.print("Heading=");
          Serial.println(heading);
          Serial.print("Roll=");
          Serial.println(euler.y());
          Serial.print("Pitch=");
          Serial.println(euler.z());
          previous_node = 5;
          break;

        }

      case 6:
        {
          // absolute sticks

          inManeuver = inSlew = false; // manual input should cancel any maneuver.

          float error;

          imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
          heading = euler.x();

          // vnew has .heading and .v elements.
          absvnew = *(CompassSticks*)radio.DATA;

          error = azerror(absvnew.heading, heading);

          long vell = (long)max(-255, min(255, error * absvnew.v * 5.0 + absvnew.v * 255.0));
          long vare = (long)max(-255, min(255, -error * absvnew.v * 5.0 + absvnew.v * 255.0));

          set_speed(mL, vell * mLpolarity);
          set_speed(mR, vare * mRpolarity);

          if (cdebug == 'c') {
            Serial.print(absvnew.heading, 1);
            Serial.print('\t');
            Serial.print(heading, 1);
            Serial.print('\t');
            Serial.print(error, 1);
            Serial.print('\t');
            Serial.print(vell);
            Serial.print('\t');
            Serial.print(vare);
            Serial.println();
          }

          previous_node = 6;

          break;
        }

      case 7:
        {
          // Closed-loop speed control mode

          inManeuver = inSlew = false; // manual input should cancel any maneuver.

          if (previous_node != 7) {
            // if just starting in "speed" mode,
            Serial.println("PI terms reset.");
            powerLpterm = powerLiterm = powerL = powerRpterm = powerRiterm = powerR = 0;
          }

          // speed target for closed-loop.

          vnew2 = *(AbsSticks*)radio.DATA;
          //          vLnew = max(vLold - dMmax * dt, min(vLold + dMmax * dt, vnew.vL));
          //          vRnew = max(vRold - dMmax * dt, min(vRold + dMmax * dt, vnew.vR));


          previous_node = 7;
          break;
        }
      case 8:
        {
          set_speed(mR, 0);
          set_speed(mL, 0);
          Serial.println("E-stop requested.");
          inManeuver = inSlew = inRove = false;
          posAt = posA;
          posRt = posR;
          blink(4);
          previous_node = 8;
          break;
        }

      case 9:
        {
          // Closed-loop translation, etc.
          tuunpack = *(TargetUpdate*)radio.DATA;
          Serial.print("Target update.");
          Serial.print('\t');
          Serial.print(tuunpack.posAt, 3);
          Serial.print('\t');
          Serial.print(tuunpack.posRt, 3);
          Serial.print('\t');
          Serial.print(tuunpack.origin);

          switch (tuunpack.origin) {
            case 'r': {

                // Relative to current position (if not moving) or current target (if in-maneuver).

                if (inManeuver) {
                  Serial.println("Extending maneuver.");
                  posAt += tuunpack.posAt;
                  posRt += tuunpack.posRt;
                } else {
                  Serial.println("New maneuver.");
                  inManeuver = true;
                  posAt = posA + tuunpack.posAt;
                  posRt = posR + tuunpack.posRt;
                }
                break;
              }

            case 'a': {
                // Set absolute target position.
                inManeuver = true;
                posAt = tuunpack.posAt;
                posRt = tuunpack.posRt;
                break;
              }

            case 'v': {
                // Set an absolute target position relative to the current vehicle frame.
                inManeuver = true;
                posAt += cos(HEADINGI) * tuunpack.posAt - sin(HEADINGI) * tuunpack.posRt;
                posRt += sin(HEADINGI) * tuunpack.posAt + cos(HEADINGI) * tuunpack.posRt;
                break;
              }

          }
          previous_node = 9;
          break;
        }

      case 12: {
          // Maintain a wheelie.
          dw = *(DoWheelie*)radio.DATA;
          float k1, k2, k3 = 0.0;
          if (dw.k1 > 998) k1 = 0; else k1 = dw.k1;
          if (dw.k2 > 998) k2 = 0; else k2 = dw.k2;
          if (dw.k3 > 998) k3 = 0; else k3 = dw.k3;

          // .x() is heading.  .y() is pitch.  .z() must be roll.
          // +y is nose up.
          float pitch = bno.getVector(Adafruit_BNO055::VECTOR_EULER).y();
          float dpitchdt = -1.0*rad_to_deg*bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE).y();

          t_start = millis();
          long t = millis();
          float theta_start = pitch;
          long odoL_start = odoL;
          int i = 0;
          float V = 0;
          int mdeadband = 30;
          float omega_critical = 0;
          float T1, T2, T3;

          //detachInterrupt(MELA);
          //detachInterrupt(MERA);

          Serial.println("[Trying to] Do a wheelie.");
          while (i < 1000) {  // Note: 1000 cycles took about 8 seconds i.e. 125 Hz.
           
            if (abs(pitch - theta_start) > 30) {
              Serial.print("Big angle error, quitting.");
              break;
            } else {}
            pitch = bno.getVector(Adafruit_BNO055::VECTOR_EULER).y();
            dpitchdt = -1.0*rad_to_deg*bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE).y();

            // The instantaneous time rate of change of pitch angle [rad/s] that would land the bot at TDC due to conservation of energy:
            // omega_critical = sqrt(2*g*(1-cos(theta))/r)
            if ((pitch - theta_start) >= 0)
              omega_critical = -1.0 * pow(2 * 9.81 * (1 - cos((pitch - theta_start) * deg_to_rad)) / 0.05, 0.5);
            else
              omega_critical =        pow(2 * 9.81 * (1 - cos((pitch - theta_start) * deg_to_rad)) / 0.05, 0.5);

            // Allow the "zero-angle" to approach the long-term average angle.  This should compensate
            // for the initial error at the point I click L2 when I'm holding the robot near its balance
            // point with my hand, and also gyro drift.
            // At the current cycle rate of ~100 Hz, the factory of 0.001 will give ~10 s time constant.
            theta_start = (pitch - theta_start) * 0.001 + theta_start;

            //T1 = (omega_critical * rad_to_deg - dpitchdt) * k1; // k1 [=] deltaV/(deg/s)
            T1 = (pitch-theta_start) * k1; // k1 [=] deltaV/(deg/s)
            T2 = (float)(odoL - odoL_start) * k2;               // k2 [=] deltaV/mm
            //T3 = dpitchdt * k3;                                 // k3 [=] deltaV/(deg/s)
            T3 = V * k3;
            //T3 = pow(V, 1.5) * k3;                                 // k3 [=] deltaV/V
            //V += T1 + T2 + T3;
            V = T1 + T2 + T3;
            //V += (int)((omega_critical * rad_to_deg - dpitchdt) * 1.1e-3 + (float)(odoL - odoL_start) * 0.001e-3 + V*0.1e-3);
            //if (V > 0) V += mdeadband; else V -= mdeadband;

            V = f->step(V);

            if (V>0) {
              set_speed(mR, (int)constrain(V+mdeadband, mdeadband, 255) * mLpolarity);
              set_speed(mL, (int)constrain(V+mdeadband, mdeadband, 255) * mRpolarity);
            } else {
              set_speed(mR, (int)constrain(V-mdeadband, -255, mdeadband) * mLpolarity);
              set_speed(mL, (int)constrain(V-mdeadband, -255, mdeadband) * mRpolarity);
            }
            i++;
            
            if (cdebug=='w') {
              if (i == 1) {
                Serial.print("T1");
                Serial.print('\t');              
                Serial.print("T2");
                Serial.print('\t');
                Serial.print("T3");
                Serial.print('\t');
                Serial.print("V");
                Serial.print('\t');
                Serial.print("theta_start");
                Serial.print('\t');
                Serial.print("pitch");
                Serial.print('\t');
                Serial.print("dpitchdt");
                Serial.println();
              }
              if (i % 10 == 0) {
                Serial.print(T1, 1);
                Serial.print('\t');              
                Serial.print(T2, 1);
                Serial.print('\t');
                Serial.print(T3, 1);
                Serial.print('\t');
                Serial.print(V);
                Serial.print('\t');
                Serial.print(theta_start, 1);
                Serial.print('\t');
                Serial.print(pitch, 1);
                Serial.print('\t');
                Serial.print(dpitchdt, 1);
                Serial.println();
              }
            }
            while ((millis()-t) < WheelieMillis) {}
            t = millis();
          }
          Serial.println("Done with wheelie.");
          Serial.print(i);
          Serial.print(" steps took ");
          Serial.print(t-t_start);
          Serial.println(" ms.");
          Serial.print("ms/step=");
          Serial.println((t-t_start)/i);
          Serial.print("f[Hz]=");
          Serial.println(i*1000/(t-t_start));
          set_speed(mR, 0);
          set_speed(mL, 0);

          //attachInterrupt(MELA, onMELAfall, FALLING);
          //attachInterrupt(MERA, onMERAfall, FALLING);
          previous_node = 12;
          break;

        }
        case 13: {
          // Start or stop roving.
          // Roving at the point amounts to driving forward until a something is detected in front of us, then turning left, then repeat...

          if (inRove) {
            Serial.println("Stopping roving.");
            inRove=false;
            inSlew=false;
            inManeuver=false;
          } else {
            Serial.println("Starting roving.");
            inRove = true;
            vRove = *(Rove*)radio.DATA;
            t_start = millis();
          }
          previous_node = 13;
          break;
        }

        case 14:
        {
          // Closed-loop slew.
          stunpack = *(SlewTarget*)radio.DATA;
          Serial.print("Slew target update.");
          Serial.print('\t');
          Serial.print(stunpack.heading, 3);
          Serial.print('\t');
          Serial.print(stunpack.origin);

          if (stunpack.origin == 'a') heading_to_target = stunpack.heading;
          else if (stunpack.origin == 'r') {
            heading_to_target = headingi + stunpack.heading;
          } else Serial.println("Slew target origin not recognized.");

          inManeuver = false;
          inSlew = true;
          previous_node = 14;
          break;
        }
    }
  }
  else {

    // No radio packet received.
    /* If we haven't received a good packet from the controller within the timeout... */
    if (millis() - lastreceipt > radiotimeout) {
      if (!inManeuver and !inSlew) {
        // ...and we're not maneuvering, stop moving.
        lastreceipt = millis();
        set_speed(mR, 0);
        set_speed(mL, 0);
        //payload = "Radio contact lost";
        //if (radio.sendWithRetry(BASE, (const void*)(&payload), sizeof(payload))) Serial.println("OK."); else Serial.println("Silence.");
        Serial.println("No receipt.");
      }

      // Take a background RSSI reading.  This works ~95% of the time.  The other 5% of the time,
      // it returns the same thing as radio.RSSI i.e. the strength of the last packet
      // received... but that was 250 ms ago!
      // passing forceTrigger=true causes a hang...
      // Also, not sure why readRSSI works perfectly in my 'r' case but takes such a long pre-silence here.
      payload.RSSIbg = radio.readRSSI();
    }
  }

  //////////////////////////////////////////////////////////////
  // Code from here down is executed every time through loop().
  //////////////////////////////////////////////////////////////

  imu::Vector<3> a = bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);

  if (a.magnitude() < 2.0) {
    // we're in low gravity.
    if (radio.sendWithRetry(BASE, "freefall.", 9, 3, 50)) Serial.println("Freefall notice ack'd."); else Serial.println("Freefall notice not ack'd.");
  }
  
  if ((a.z() < 0.0) and (a.magnitude() > 8.0)) {
    // we're not right-side up.
    if (radio.sendWithRetry(BASE, "tipped.", 7, 3, 50)) Serial.println("Tipped notice ack'd."); else Serial.println("Tipped notice not ack'd.");
  }

  if (sendpos && (millis() - lastsend > PositionUpdatetimeout)) {
    lastsend = millis();

    //payload.RSSIRx = radio.RSSI;
    payload.Vbat = Vbat();
    payload.posA = posA;
    payload.posR = posR;
    imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
    payload.heading = euler.x(); // Magnetic heading, not dead-reckoned.
    payload.cmclearahead = NewPing::convert_cm(sonar0.ping_median());
    payload.cmclearleft = NewPing::convert_cm(sonar1.ping_median());

    if (radio.sendWithRetry(BASE, (const void*)(&payload), sizeof(payload), 3, 50)) Serial.println("OK."); else Serial.println("Silence.");
  }

  if (inRove) {
    long cm_to_obstacle = NewPing::convert_cm(sonar0.ping_median());
    if (cm_to_obstacle==0) cm_to_obstacle = 100;
    if ((millis()-t_start)/1000 > vRove.t) {
      Serial.println("Roving duration expired.");
      inRove=false;
      posAt = posA;
      posRt = posR;
    } else {
      if (cm_to_obstacle < 20) {
        Serial.print("Obstacle detected at ");
        Serial.print(cm_to_obstacle);
        Serial.print(" cm.  Changing tack."); 
        Serial.println();
        
        inManeuver = false;
        inSlew = true;

        heading_to_target = headingi + 90;
        
        //forward_tack = 0;
        //side_tack = 0.500;

      } else {
        float forward_tack, side_tack = 0;
  
        inManeuver = true;
        inSlew = false;

        //vnew2.vL=vRove.V;
        //vnew2.vR=vRove.V;
        forward_tack = 1.0;
        side_tack = 0;

        posAt = posA + cos(HEADINGI) * forward_tack - sin(HEADINGI) * side_tack;
        posRt = posR + sin(HEADINGI) * forward_tack + cos(HEADINGI) * side_tack;

      }
    }
  }

  if (inSlew) {
    // Slew to target heading.
    heading_error = azerror(heading_to_target, headingi); // [deg] to_target - facing.  positive error means we need to turn to right (CW).
 
    if (abs(heading_error) <= 3.0) {
    
      // Yay! We got close enough to target to stop.
      set_speed(mR, 0);
      set_speed(mL, 0);
      Vtarget = 0;
      inSlew = false;
      Serial.println("Slew target reached.  Slew ended.");
      blink(1);
    } else {
    
      Vtarget = 0.5; //go-go factor [0,1].
    
    }

    vnew2.vL = (long)max(-maxVd, min(maxVd, heading_error * Vtarget * 10.0));
    vnew2.vR = (long)max(-maxVd, min(maxVd, -heading_error * Vtarget * 10.0));

    if (inRove) {
      vnew2.vL = constrain(vnew2.vL, -vRove.V, vRove.V);
      vnew2.vR = constrain(vnew2.vR, -vRove.V, vRove.V);
    }
  }

  if (inManeuver) {

    // If here, assume that posAt and posRt are target position to drive toward.
    // We assign values to vnew2.vL and vnew2.vR (as if an AbsSticks was received) in order to drive there.

    heading_to_target = atan2(posRt - posR, posAt - posA) * rad_to_deg; // Compass heading from here to target [deg].

    heading_error = azerror(heading_to_target, headingi); // [deg] to_target - facing.  positive error means we need to turn to right (CW).

    poserrmag = sqrt(pow(posRt - posR, 2) + pow(posAt - posA, 2)); // Distance from the target position [m].

    if (cdebug == 'M') {
      Serial.print(heading_error, 1);
      Serial.print('\t');
      Serial.print(poserrmag, 3);
      Serial.println();
    }

    if (poserrmag <= 0.050) {

      // Yay! We got close enough to target to stop.
      set_speed(mR, 0);
      set_speed(mL, 0);
      Vtarget = 0;
      inManeuver = false;
      Serial.println("Target reached.  Maneuver ended.");
      blink(1);
    } else {

      Vtarget = poserrmag * 1.5; //go-go factor [0,1].

    }

    vnew2.vL = (long)max(-maxVd, min(maxVd, heading_error * Vtarget * 10.0 + Vtarget * 255.0));
    vnew2.vR = (long)max(-maxVd, min(maxVd, -heading_error * Vtarget * 10.0 + Vtarget * 255.0));

    if (inRove) {
      vnew2.vL = constrain(vnew2.vL, -vRove.V, vRove.V);
      vnew2.vR = constrain(vnew2.vR, -vRove.V, vRove.V);
    }

  }

  if (inManeuver or inSlew or (previous_node == 7)) {
    // Closed-loop speed control
    // Compute left and right PWM fraction from previous measured speed.

    // Speed error
    //    applied motor voltage frac =
    //             demand speed frac -
    //                            measured speed / speed limit (i.e. measured speed frac)
    errorL = vnew2.vL / 255. - vLms / vmax;
    errorR = vnew2.vR / 255. - vRms / vmax;

    powerLpterm = errorL * kp;
    powerLiterm += errorL * ki;
    if ((abs(vLms) < 0.005) and (abs(errorL) < 0.01)) {
      powerLiterm = 0; // integral term reset.
    }
    if (abs(vnew2.vL) < 20) {
      powerLiterm = 0; // integral term reset.
    }
    powerL = powerLpterm + powerLiterm;
    powerL = (powerL - powerLprev) * alpha + powerLprev;
    powerLprev = powerL;

    powerRpterm = errorR * kp;
    powerRiterm += errorR * ki;
    if ((abs(vRms) < 0.005) and (abs(errorR) < 0.01)) {
      powerRiterm = 0; // integral term reset.
    }
    if (abs(vnew2.vR) < 20) {
      powerRiterm = 0; // integral term reset.
    }
    powerR = powerRpterm + powerRiterm;
    powerR = (powerR - powerRprev) * alpha + powerRprev;
    powerRprev = powerR;

    powerL = constrain(powerL, -1.0, 1.0);
    powerR = constrain(powerR, -1.0, 1.0);

    set_speed(mL, (long)(powerL * 254.0 * mLpolarity));
    set_speed(mR, (long)(powerR * 254.0 * mRpolarity));
  }

  // Poor man's IMU-based dead reckoning.
  if ((millis() - oldtIMU) > dtIMUtarget) {
    dtIMUactual = millis() - oldtIMU; // [ms]
    Vx += bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL).x() * dtIMUactual / 1000.0; // Velocity delta is forward acceleration times duration since previous sample [m/s].  +y appears to be robot-forward.
    headingi = bno.getVector(Adafruit_BNO055::VECTOR_EULER).x();

    posAi += Vx * dtIMUactual / 1000.0 * cos(headingi * deg_to_rad);
    posRi += Vx * dtIMUactual / 1000.0 * sin(headingi * deg_to_rad);

    oldtIMU = millis();
  }

  if (digitalRead(RFM69_INT)) { // manual poll of radio interrupt pin for my NOINTERRUPTS version.
    radio.isr0();  // If RFM69 interrupt pin is high, call "ISR".
  }

  // If measured speed hasn't changed in several samples, assume it's because encoder stopped moving.
  vLms = vLm; // Sample measured velocity synchronous to this main loop.
  vRms = vRm; // Sample measured velocity synchronous to this main loop.
  if (odoL == odoLprev) {
    vLmNC++;
    if (vLmNC > 3) {
      vLms = vLm = 0;
    }
  } else {
    vLmNC = 0;
  }
  if (odoR == odoRprev) {
    vRmNC++;
    if (vRmNC > 3) {
      vRms = vRm = 0;
    }
  } else {
    vRmNC = 0;
  }
  vLms = alphav * (vLms - vLmsprev) + vLmsprev;
  vRms = alphav * (vRms - vRmsprev) + vRmsprev;
  odoLprev = odoL;
  odoRprev = odoR;
  vLmsprev = vLms;
  vRmsprev = vRms;

  // If measured speed is less than a certain magnitude, assume it's because encoder stopped moving.
  if (abs(vLm) < vmin) {
    vLm = 0;
  }
  if (abs(vRm) < vmin) {
    vRm = 0;
  }

  if (Serial.available() > 0) {
    cdebug = Serial.read();
    Serial.println(cdebug);
  }

  if (cdebug == '?' or cdebug == 'h') {
    Serial.println("Debug: g-GPS l-left encoder v-velocity i-IMU r-radio c-compass mode o-odometer d-dead reckoning L-linear acceleration u-ultrasonic");
  }

  if (cdebug == 'r') {
    // Radio stuff
    Serial.print(radio.RSSI);
    Serial.print('\t');
    Serial.print(radio.readRSSI());
    Serial.print('\t');
    Serial.print(dt);
    Serial.print('\t');
    Serial.print(lastreceipt);
    if (previous_node == 2 or previous_node == 8) {
    }
    Serial.println();
  }

  if (cdebug == 'g') {
    // GPS stuff.
    Serial.println(GPS.lastNMEA());
  }

  if (cdebug == 'l') {

    // Encoder and motor velocity stuff (left-side only).
    Serial.print((*(int*)0x60000020), BIN); // Port A bits.
    Serial.print('\t');
    Serial.print(odoL);  // net number of ticks forward.
    Serial.print('\t');
    Serial.print(vLmNC); // number of times with No Change.
    Serial.print('\t');
    Serial.print(vLm, 4); // measured track velocity [m/s].
    Serial.print('\t');
    Serial.print(vLms, 4); // measured track velocity (synchronous) [m/s].
    Serial.print('\t');
    Serial.print(vLmsprev, 4); // previous vLms [m/s].
    Serial.print('\t');
    Serial.print(vnew.vL); // track "speed demand" [-255,255].
    Serial.print('\t');
    Serial.print(powerLpterm, 3); // fraction of max motor effort to apply [-1.0,1.0].
    Serial.print('\t');
    Serial.print(powerLiterm, 3); // fraction of max motor effort to apply [-1.0,1.0].
    Serial.print('\t');
    Serial.print(powerL, 3); // fraction of max motor effort to apply [-1.0,1.0].
    Serial.println();
  }

  if (cdebug == 'o') {
    // Odometer-based dead reckoning
    Serial.print(odoL);
    Serial.print("\t");
    Serial.print(odoR);
    Serial.print("\t");
    Serial.print(headingi, 1);
    Serial.print("\t");
    Serial.print(posA, 3);
    Serial.print("\t");
    Serial.print(posR, 3);
    Serial.print("\t");
    Serial.print(posAt, 3);
    Serial.print("\t");
    Serial.print(posRt, 3);
    Serial.println();
  }

  if (cdebug == 'd') {
    // IMU-based dead reckoning
    Serial.print(Vx);
    Serial.print("\t");
    Serial.print(headingi);
    Serial.print("\t");
    Serial.print(posAi);
    Serial.print("\t");
    Serial.print(posRi);
    Serial.println();
  }

  if (cdebug == 'v') {

    // Encoder and motor velocity stuff.
    Serial.print(vLm);
    Serial.print('\t');
    Serial.print(vRm);
    Serial.print("\t\t");
    Serial.print(vnew.vL);
    Serial.print('\t');
    Serial.print(vnew.vR);
    Serial.print("\t\t");
    Serial.print(powerL);
    Serial.print('\t');
    Serial.print(powerR);
    Serial.println();
  }
  if (cdebug == 'i') {

    // IMU stuff.  Send "i" from the Arduino serial monitor to switch this on.  Send anything else to switch it off.

    uint8_t overall, gyro, accel, mag;
    overall = gyro = accel = mag = 0;
    bno.getCalibration(&overall, &gyro, &accel, &mag);
    imu::Vector<3> a = bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
    imu::Vector<3> m = bno.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER);
    imu::Vector<3> g = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);
    imu::Vector<3> e = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
    imu::Vector<3> l = bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);
    imu::Vector<3> n = bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY); // n for Newton.

    Serial.print(overall);
    Serial.print('\t');
    Serial.print(gyro);
    Serial.print('\t');
    Serial.print(accel);
    Serial.print('\t');
    Serial.print(mag);
    Serial.print('\t');
    Serial.print(a.x());
    Serial.print('\t');
    Serial.print(a.y());
    Serial.print('\t');
    Serial.print(a.z());
    Serial.print('\t');
    Serial.print(m.x());
    Serial.print('\t');
    Serial.print(m.y());
    Serial.print('\t');
    Serial.print(m.z());
    Serial.print('\t');
    Serial.print(g.x());
    Serial.print('\t');
    Serial.print(g.y()*-1.0*rad_to_deg);
    Serial.print('\t');
    Serial.print(g.z());
    Serial.print('\t');
    Serial.print(e.x());
    Serial.print('\t');
    Serial.print(e.y());
    Serial.print('\t');
    Serial.print(e.z());
    Serial.print('\t');
    Serial.print(l.x());
    Serial.print('\t');
    Serial.print(l.y());
    Serial.print('\t');
    Serial.print(l.z());
    Serial.print('\t');
    Serial.print(n.x());
    Serial.print('\t');
    Serial.print(n.y());
    Serial.print('\t');
    Serial.print(n.z());
    Serial.println();
  }
  if (cdebug == 'L') {

    // Linear acceleration.

    uint8_t overall, gyro, accel, mag = 0;
    bno.getCalibration(&overall, &gyro, &accel, &mag);
    imu::Vector<3> l = bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);
    Serial.print(accel);
    Serial.print('\t');
    Serial.print(l.x());
    Serial.print('\t');
    Serial.print(l.y());
    Serial.print('\t');
    Serial.print(l.z());
    Serial.println();
  }
  if (cdebug == 'u') {
    // Ultrasonic rangers.
    Serial.print(NewPing::convert_cm(sonar0.ping_median()));
    Serial.print('\t');
    Serial.print(NewPing::convert_cm(sonar1.ping_median()));
    Serial.println();
  }
}
