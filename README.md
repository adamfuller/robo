# Warnings #
1. You need to modify the Arduino RFM69 library to remove interrupts.  Sorry but I haven't documented this.  Get in touch if you want to know more.
2. I've made some edits to Wes's RFM69 Python library regarding hard-coded pin numbers.
3. See my fork repository for these changes, I will submit a PR to Wes soon hopefully.

# Overview #
This is a software "kit" for a simple two-track robot.  It includes an Arduino program for the robot itself and Python code to run on a Beaglebone Black providing the radio control "base station".

The [RFM69 radio](https://cdn.sparkfun.com/datasheets/Wireless/General/RFM69HCW-V1.1.pdf) is used.

# Usage #
1. Solder and wire everything up.  If you're like me this takes several Saturday afternoons.
2. On the Beaglebone, install [Wes's Python port of the RFM69 library](https://github.com/wcalvert/rfm69-python).  Thanks Wes!
3. Put `sticks.py` onto your BBB and run it.
4. Upload the `tracks.ino` sketch onto your Feather.  If you're using the Arduino IDE (I recommend it), you may need to rename the parent folder to `tracks`.

Sometimes I want the Beaglebone to run `sticks.py` automatically on startup, for example if it's powered by a USB power bank I therefore have no way to log in.  For that, I ran `crontab -e` and added `@reboot /root/dev/radio/sticks.py`.  Make sure `sticks.py` is executable.

# Bill of materials #
Essentials:

* Beaglebone Black

![Link Text](http://elinux.org/images/2/23/REV_A5A.jpg)

* A USB gamepad/joystick.

![Playstation Dual Shock and Nintendo 64 controller to USB adaptor](https://s3-us-west-2.amazonaws.com/usedphotosna/39736395_614.jpg)
+
![Sony Dual Shock controller](https://upload.wikimedia.org/wikipedia/en/b/b6/Transparent_Blue_DualShock.png)

* RFM69HCW breakout from Adafruit

![RFM69 breakout from Adafruit](https://cdn-learn.adafruit.com/assets/assets/000/031/768/medium800/adafruit_products_3070_iso_ORIG.jpg?1460751712)


* Feather M0 with RFM69HCW

![Adafruit Feather RFM69](https://cdn-learn.adafruit.com/assets/assets/000/031/614/medium800/feather_3077_iso_demo_ORIG.jpg?1459979081)

* Feather DC motor wing

![Adafruit DC motor Wing](https://cdn-shop.adafruit.com/970x728/2927-02.jpg)

* Gear motors (2)

* Quadrature encoder (1 per motor)

![Pololu 6-volt motor with encoder](https://a.pololu-files.com/picture/0J5832.1200.jpg?1a7afd1aeb0cd22d610efcf66847279c)

* Pololu Zumo chassis *

 ![Two mini sumo robots fighting](https://mcuoneclipse.files.wordpress.com/2013/09/sumo-robot-fight.png)